using Test
    function test()
        matrix_pot([1 2; 3 4],1) == [1 2; 3 4]
        matrix_pot([1 2;3 4],2) == [7.0 10;15.0 22.0]
        matrix_pot([1 2;3 4],3) == [37.0 54.0; 81.0 118.0] 
        matrix_pot_by_squaring([1 2;3 4],3) == [37.0 54.0; 81.0 118.0]
        matrix_pot_by_squaring([1 2; 3 4],1) == [1 2; 3 4]
        matrix_pot_by_squaring([1 2;3 4],2) == [7.0 10;15.0 22.0]
        println("Testes Acabaram!")
end


function multiplica(a, b) #Função de multiplicar matrizes vista em aula
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
      return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
            c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
atual = M
    if p == 1 
        return M
    end
    for i in 1:p-1
    atual = multiplica(atual,M)
    end
return atual
end

function matrix_pot_by_squaring(M, p)
    if  p == 1
        return M
    elseif par(p) == true
       return matrix_pot_by_squaring(multiplica(M,M),p/2)
    elseif impar(p) == true
       return multiplica(M , matrix_pot_by_squaring(multiplica(M,M),(p-1)/2))
    end
end

function par(n)
    if n % 2 == 0
        return true
    else
        return false
    end
end

function impar(n)
    if n % 2 == 1
        return true
    else
        return false
    end
end

using LinearAlgebra
function compare_times()
M = Matrix(LinearAlgebra.I, 30, 30)
@time matrix_pot(M, 10)
@time matrix_pot_by_squaring(M, 10)
end

compare_times()
test()